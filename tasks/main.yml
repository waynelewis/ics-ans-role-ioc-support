---
- name: install required packages
  package:
    name: "{{ ioc_packages + ioc_dev_packages + ioc_prometheus_exporter_packages }}"
    state: present

- name: create the group for the ioc user
  group:
    name: "{{ ioc_group_name }}"
    gid: "{{ ioc_group_id }}"

- name: create the ioc user for running iocs
  user:
    name: "{{ ioc_user_name }}"
    uid: "{{ ioc_user_id }}"
    group: "{{ ioc_group_name }}"
    system: true

- name: get group entries
  getent:
    database: group

- name: add the ioc user to the realtime group
  user:
    name: "{{ ioc_user_name }}"
    groups: realtime
    append: true
  when: "'realtime' in getent_group"

# See https://epics-controls.org/resources-and-support/documents/howto-documents/channel-access-reach-multiple-soft-iocs-linux/
# We use ifup-local because NetworkManager is not enabled
- name: create script to convert UDP CA incoming requests to broadcast
  template:
    src: ifup-local.j2
    dest: /sbin/ifup-local
    owner: root
    group: root
    mode: 0755
  tags:
    - ioc-iptables
  notify:
    - run ifup-local

- name: create /var/log/procServ directory
  file:
    path: /var/log/procServ
    owner: "{{ ioc_user_name }}"
    group: "{{ ioc_group_name }}"
    mode: 0755
    state: directory

- name: configure logrotate for procServ
  copy:
    src: procserv
    dest: /etc/logrotate.d/
    owner: root
    group: root
    mode: 0644

- name: create prometheus exporter user
  user:
    name: "{{ ioc_prometheus_exporter_user }}"
    system: true

- name: create prometheus exporter directory
  file:
    path: "{{ ioc_prometheus_exporter_directory }}"
    owner: "{{ ioc_prometheus_exporter_user }}"
    mode: 0755
    state: directory

- name: add prometheus exporter script
  copy:
    src: e3-exporter.py
    dest: "{{ ioc_prometheus_exporter_script }}"
    owner: "{{ ioc_prometheus_exporter_user }}"
    mode: 0744

- name: add prometheus exporter configuration file
  copy:
    content: "{{ iocs | to_nice_yaml }}"
    dest: "{{ ioc_prometheus_exporter_config }}"
    owner: "{{ ioc_prometheus_exporter_user }}"
    mode: 0644
  tags:
    - ioc-update
  notify:
    - Restart E3 exporter

- name: create service for prometheus exporter
  template:
    src: "e3-exporter.service.j2"
    dest: "/etc/systemd/system/e3-exporter.service"
    owner: root
    group: root
    mode: 0644
  notify:
    - reload systemd config
    - Restart E3 exporter

- name: Flush handlers
  meta: flush_handlers
  tags:
    - ioc-update

- name: Ensure prometheus exporter service is enabled and started
  service:
    name: e3-exporter
    state: started
    enabled: true

- name: Set environment variable fact
  set_fact:
    ioc_env:
      AS_TOP: "{{ ioc_nonvolatile_path }}"
      LOG_SERVER_NAME: "{{ ioc_log_server_name | default(omit) }}"
      ERRORLOG_SERVER_PORT: "{{ ioc_errorlog_server_port | default(omit) }}"
      CAPUTLOG_SERVER_PORT: "{{ ioc_caputlog_server_port | default(omit) }}"
      ASG_FILENAME: "{{ ioc_asg_filename | default(omit) }}"
      FACNAME: "{{ ioc_facility_name | default(omit) }}"
  tags:
    - ioc-update

# - name: remove IOCs
# include_tasks: remove_ioc.yml
# tags:
# - ioc-update

- name: create IOCs
  include_tasks: deploy_ioc.yml
  tags:
    - ioc-update
