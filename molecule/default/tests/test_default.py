import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("ioc_hosts")


def test_conserver(host):
    conserver = host.service("conserver")
    assert conserver.is_enabled
    assert conserver.is_running
    if host.ansible.get_variables()["inventory_hostname"] == "ics-ans-role-ioc-support-default":
        cmd = host.run("console -u")
        assert cmd.rc == 0
        assert len(cmd.stdout.splitlines()) == 2
        for line in cmd.stdout.splitlines():
            assert line.split()[0] in ["test-IOC", "test-IOC_2"]
            assert line.split()[1] == "up"


def test_iocs(host):
    if host.ansible.get_variables()["inventory_hostname"] == "ics-ans-role-ioc-support-default":
        for ioc in ["test-IOC", "test-IOC_2"]:
            ioc_service = host.service("ioc@" + ioc)
            assert ioc_service.is_enabled
            assert ioc_service.is_running


def test_ioc_packages(host):
    assert host.package("procServ").is_installed
    if host.ansible.get_variables()["inventory_hostname"] == "ics-ans-role-ioc-support-default":
        assert not host.package("tmux").is_installed
    else:
        assert host.package("tmux").is_installed


def test_activation_script(host):
    if host.ansible.get_variables()["inventory_hostname"] == "ics-ans-role-ioc-support-default":
        script = host.file(
            "/home/iocuser/.conda/envs/test-IOC/etc/conda/activate.d/ioc_activate.sh"
        )
        assert script.exists
        assert script.contains('export LOG_SERVER_NAME="logs.example.com"')
        assert script.contains('export CUSTOM_VAR="test-ioc"')
        assert script.contains('export IOCNAME="test-IOC"')


def test_iocuser(host):
    user = host.user("iocuser")
    assert user.uid == 1042
    if host.ansible.get_variables()["inventory_hostname"] == "ics-ans-role-ioc-support-no-iocs":
        assert "realtime" in user.groups


def test_prometheus_exporter(host):
    if host.ansible.get_variables()["inventory_hostname"] == "ics-ans-role-ioc-support-default":
        cmd = host.command("curl http://localhost:12110")
        assert 'e3_repository_dirty{ioc="test-IOC"} 0.0' in cmd.stdout
        assert 'e3_repository_dirty{ioc="test-IOC:2"} 0.0' in cmd.stdout
        assert 'e3_repository_local_commits{ioc="test-IOC"} 0.0' in cmd.stdout
        assert 'e3_repository_local_commits{ioc="test-IOC:2"} 0.0' in cmd.stdout
